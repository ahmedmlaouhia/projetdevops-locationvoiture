FROM php:8.2.0

WORKDIR /app

RUN apt-get update && apt-get install -y \
    libicu-dev \
    zlib1g-dev \
    libzip-dev \
    git \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install intl \
    && docker-php-ext-install zip \
    && rm -rf /var/lib/apt/lists/*

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

COPY composer.json /app/

RUN composer install --no-scripts --no-autoloader --no-progress

COPY . /app

RUN composer dump-autoload --no-scripts --optimize

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

EXPOSE 8000

CMD ["php", "-S", "0.0.0.0:8000", "-t", "public"]
