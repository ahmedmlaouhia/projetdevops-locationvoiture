<?php

namespace App\Tests;

use Symfony\Component\Panther\PantherTestCase;

class FunctionalTest extends PantherTestCase
{

    public function testShouldRegisterNewUser()
    {
        $client = static::createPantherClient();
        $crawler = $client->request('GET', '/register');
        $buttonCrawlerNode = $crawler->selectButton('Register');
        $form = $buttonCrawlerNode->form();
        $uuid = uniqid();
        $form = $buttonCrawlerNode->form([
            'registration_form[email]' => 'test' . $uuid . '@test.com',
            'registration_form[roles]' => 'ROLE_CLIENT',
            'registration_form[agreeTerms]' => true,
            'registration_form[plainPassword]' => 'test1234',
        ]);
        $client->submit($form);
        $this->assertResponseRedirects('/login');
    }

    public function testShouldDisplayLoginForm()
    {
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Please sign in');
    }

    public function testShouldLoginUser()
    {
        $client = static::createPantherClient();
        $crawler = $client->request('GET', '/login');
        $buttonCrawlerNode = $crawler->selectButton('Sign in');
        $form = $buttonCrawlerNode->form();
        $form = $buttonCrawlerNode->form([
            'email' => 'ahmed@gmail.com',
            'password' => 'test1234',
        ]);
        $client->submit($form);
        $this->assertResponseRedirects('/demo');
    }
}
